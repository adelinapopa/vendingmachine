package com.vendingmachine.controlpanel;

import com.vendingmachine.machine.IVendingMachine;
import com.vendingmachine.machine.product.Product;

public class ControlPanel {

	// dependency injection = vom injecta un modul in alt modul ca dependenta si
	// serverul va gestiona instantierea( el va face new)
	//
	private IVendingMachine iVendingMachine;

	public IVendingMachine getiVendingMachine() {
		return iVendingMachine;
	}

	public void setiVendingMachine(IVendingMachine iVendingMachine) {
		this.iVendingMachine = iVendingMachine;
	}

	public Product dispenceProduct(int sheelfCode) {

		return iVendingMachine.dispanceProduct(sheelfCode);
	}

}
