package com.vendingmachine.machine;

import java.util.Queue;

import com.vendingmachine.bank.Money;
import com.vendingmachine.machine.product.Product;

public interface IVendingMachine {
	public abstract Product dispanceProduct(int shelfCode);

	public boolean payProductPrice(int shelfCode, double amountPaid);

	public Queue<Money> computeChange(double amountReceived, double amountExpected);

	public boolean validateAmountReceived(Queue<Money> moneyReceived);

}
